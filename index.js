const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const EventHandler = require('./src/EventHandler');

// Dummy index test file
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

// Socket operations
io.on('connection', (socket) => {
  EventHandler.handleEvents(io, socket);
});

// Inicia o server
server.listen(process.env.PORT || 3000);