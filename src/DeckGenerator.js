const Card = require('./Card');
const Deck = require('./Deck');
const Fs = require('fs');

let cardLanguage = ["pt_br"];
let expansion = ["regular"];
let Gamecards = {};

function jsonReader(filePath, cb) {
    Fs.readFile(filePath, (err, fileData) => {
        if (err) {
            return cb && cb(err)
        }
        try {
            const object = JSON.parse(fileData)
            return cb && cb(null, object)
        } catch (err) {
            return cb && cb(err)
        }
    })
}

cardLanguage.forEach(language => {
    expansion.forEach(expansion => {
        jsonReader(`./src/Cards/${language}/${expansion}.json`, (err, items) => {
            if (err) {
                console.log(err)
                return
            }
            Gamecards = items.cards;
            return 0;
        })
    })
})

class DeckGenerator {
    static generate() {
        let deck = new Deck();

        Gamecards.verbs.forEach(card => {
            deck.addCard(new Card(card.name, card.points, card.expansion))
        });
        Gamecards.substantives.forEach(card => {
            deck.addCard(new Card(card.name, card.points, card.expansion))
        });
        Gamecards.adjectives.forEach(card => {
            deck.addCard(new Card(card.name, card.points, card.expansion))
        });

        return deck;
    }
}

module.exports = DeckGenerator;