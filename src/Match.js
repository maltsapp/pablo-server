const Player = require('./Player');

class Match {
    maxTableCards = 5;
    maxCardsInHand = 5;

    constructor(deck) {
        this.originalDeck = deck;

        this.resetMatch(deck);
    }

    start() {
        if (this.isLive()) {
            return false;
        }

        this.turns = 1;
        this.deck.shuffle();
        this.refillTable();
        this.getCurrentPlayer();

        this.players.forEach((p) => {
            for (let i = 0; i < this.maxCardsInHand; ++i) {
                this.playerDraw(p.name);
            }
        });

        return true;
    }

    end() {
        if (!this.isLive()) {
            return false;
        }

        this.resetDeck();
        this.resetMatch(this.originalDeck);
    }

    resetMatch(deck) {
        this.deck = deck;
        this.players = [];
        this.tableCards = [];
        this.playedCards = [];
        this.discardedCards = [];
        this.currentPlayer = null;
        this.currentPlayerIndex = 0;
        this.turns = 0;
    }

    isLive() {
        return this.turns > 0;
    }

    refillTable() {
        while (this.tableCards.length < this.maxTableCards) {
            this.tableCards.push(this.drawCard());
        }
    }

    resetDeck() {
        this.deck.reset(this.playedCards.concat(this.discardedCards));
    }

    endTurn() {
        this.refillTable();
        this.getCurrentPlayer();
        this.turns += 1;
    }

    drawCard() {
        if (this.deck.isEmpty()) {
            this.resetDeck();
        }

        return this.deck.draw();
    }

    addPlayer(playerName) {
        let player = this.getPlayer(playerName);

        if (!player) {
            this.players.push(new Player(playerName));
            return true;
        }

        return false;
    }

    playerDraw(playerName, cardName = null) {
        let player = this.getPlayer(playerName);

        if (player && player.hand.length < this.maxCardsInHand) {
            if (cardName) {
                let card = this.drawCardFromTable(cardName);

                if (card) {
                    player.draw(card);
                    this.refillTable();
                }
            } else {
                player.draw(this.drawCard());
                return true;
            }
        }

        return false;
    }

    playCard(playerName, cardName) {
        let player = this.getPlayer(playerName);

        if (player) {
            let card = player.play(cardName);
            if (card) {
                this.playedCards.push(card);
                return true;
            }
        }

        return false;
    }

    getPlayer(playerName) {
        return this.players.find(p => p.name === playerName);
    }

    drawCardFromTable(cardName) {
        let card = this.tableCards.find(c => c.word === cardName);

        this.tableCards = this.tableCards.filter(c => c.word !== cardName);
        return card;
    }

    getCurrentPlayer() {
        if (this.players.length > 0) {
            if (this.currentPlayerIndex > this.players.length - 1) {
                this.currentPlayerIndex = 0;
            }

            this.currentPlayer = this.players[this.currentPlayerIndex++];
        } else {
            this.currentPlayer = null;
        }

        return this.currentPlayer;
    }

    getWinner() {
        return [...this.players]
            .sort((a, b) => (a.score < b.score) ? -1 : (a.score > b.score ? 1 : 0))
            .reverse()
            .shift();
    }
}

module.exports = Match;