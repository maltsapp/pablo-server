class Deck {
    cards = [];

    addCard(card) {
        this.cards.push(card);
    }

    draw() {
        if (this.isEmpty()) {
            return null;
        }

        return this.cards.pop();
    }

    isEmpty() {
        return this.cards.length === 0;
    }

    reset(remaingCards) {
        this.cards = [...remaingCards];
        this.shuffle();
    }

    /**
     * Copied from https://bost.ocks.org/mike/shuffle/
     */
    shuffle() {
        var m = this.cards.length, t, i;

        // While there remain elements to shuffle…
        while (m) {

            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = this.cards[m];
            this.cards[m] = this.cards[i];
            this.cards[i] = t;
        }
    }
}

module.exports = Deck;
