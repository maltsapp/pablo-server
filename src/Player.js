class Player {
    hand = [];
    score = 0;

    constructor(name) {
        this.name = name;
    }

    draw(card) {
        this.hand.push(card);
    }

    play(cardName) {
        let card = this.findCardInHand(cardName);

        if (card) {
            this.removeCardFromHand(cardName);

            // Update player score
            this.score += parseInt(card.points);

            return card;
        }

        return null;
    }

    discard(cardName) {
        let card = this.findCardInHand(cardName);

        if (card) {
            this.removeCardFromHand(cardName);

            return card;
        }

        return null;
    }

    findCardInHand(cardName) {
        return this.hand.find(c => c.word === cardName);
    }

    removeCardFromHand(cardName) {
        this.hand = this.hand.filter(c => c.word !== cardName);
    }
}

module.exports = Player;