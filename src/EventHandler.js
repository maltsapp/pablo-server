const DeckGenerator = require('./DeckGenerator');
const Match = require('./Match');
const Player = require('./Player');

class EventHandler {
    static room = 'default';
    static message = '';
    static match;

    static handleEvents(io, socket) {
        // Add socket to room
        socket.join(this.room);

        // - Enter room
        socket.on('join', (playerName) => {
            if (!this.match) {
                this.startNewMatch();
            }

            if (this.match.addPlayer(playerName)) {
                this.message = `Usuário ${playerName} acabou de entrar!`;
            } else {
                this.message = `Usuário ${playerName} está de volta!`;
            }

            this.sendResponse(io, 'join');
        });

        // - Start game
        socket.on('start', () => {
            if (!this.match) {
                this.startNewMatch();
            }

            if (this.match.players.length === 0) {
                // TODO: Evento deve ser de erro...
                this.message = `Ainda não há jogadores na partida!`;
            } else if (this.match.start()) {
                this.message = `A partida vai começar!`;
            } else {
                // TODO: Evento deve ser de erro...
                this.message = `Partida já iniciada!`;
            }

            this.sendResponse(io, 'start');
        });

        // - Card played
        socket.on('cardPlayed', (playerName, cardName) => {
            if (!this.isLive()) {
                return this.sendErrorGameIsNotLive();
            }

            if (this.match.playCard(playerName, cardName)) {
                this.message = `O jogador ${playerName} jogou a carta ${cardName}!`;
            } else {
                // TODO: Evento deve ser de erro...
                this.message = `O jogador ${playerName} fez uma jogada ilegal!`;
            }

            this.sendResponse(io, 'cardPlayed');
        });

        // - Next song
        socket.on('next', () => {
            if (!this.isLive()) {
                return this.sendErrorGameIsNotLive();
            }

            this.match.endTurn();
            this.message = `Um novo turno começou! Próximo a jogar: ${this.match.currentPlayer.name}`;

            this.sendResponse(io, 'next');
        });

        // - Draw from deck
        socket.on('drawDeck', (playerName) => {
            if (!this.isLive()) {
                return this.sendErrorGameIsNotLive();
            }

            if (this.match.playerDraw(playerName)) {
                this.message = `O jogador ${playerName} pescou uma carta!`;
            } else {
                // TODO: Evento deve ser de erro...
                this.message = `O jogador ${playerName} não pode pescar mais nenhuma carta!`;
            }

            this.sendResponse(io, 'drawDeck');
        });

        // - Draw from table
        socket.on('drawTable', (playerName, cardName) => {
            if (!this.isLive()) {
                return this.sendErrorGameIsNotLive();
            }

            if (this.match.playerDraw(playerName, cardName)) {
                this.message = `O jogador ${playerName} pegou a carta ${cardName} da mesa!`;
            } else {
                // TODO: Evento deve ser de erro...
                this.message = `O jogador ${playerName} não pode pescar mais nenhuma carta!`;
            }

            this.sendResponse(io, 'drawTable');
        });

        // - End game
        socket.on('end', () => {
            if (!this.isLive()) {
                return this.sendErrorGameIsNotLive();
            }

            this.message = `O jogo terminou! ${(this.match.getWinner() || {}).name} é o vencedor!`;
            this.sendResponse(io, 'end');
            this.match.end();
        });
    }

    static isLive() {
        return this.match && this.match.isLive();
    }

    static startNewMatch() {
        this.match = new Match(DeckGenerator.generate());
    }

    static sendErrorGameIsNotLive() {
        this.message = `Um novo jogo ainda não começou!`;

    }

    static sendResponse(io, event) {
        io.sockets.in(this.room).emit(event, {
            message: this.message,
            match: this.match
        });
    }
}

module.exports = EventHandler;