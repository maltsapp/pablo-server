class Card {
    constructor(word, points, expansion) {
        this.word = word;
        this.points = points;
        this.expansion = expansion;
    }
}

module.exports = Card;